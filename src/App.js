import { Component } from "react";
import "./App.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import Profile from "./components/Profile";

import Loader from "./components/Loader";
import NavBar from "./components/NavBar";

import CompanyList from "./components/CompanyList";

import JobList from "./components/JobList";
import PageNotFound from "./components/PageNotFound";
import Login from "./components/Login";
class App extends Component {
  state = { loading: false };

  /*   onJobClick = (e) => {
    this.setState({ selectedJob: e });
  }; */

  componentDidMount() {
    this.loaderStart();
  }
  loaderStart() {
    this.setState({ loading: true });
    setTimeout(() => {
      this.setState({ loading: false });
    }, 1000);
  }

  render() {
    return (
      <>
        <div className='App ui container-fluid'>
          {this.state.loading && (
            <div style={{ height: "100vh" }}>
              <Loader />
            </div>
          )}
          {!this.state.loading && (
            <>
              <BrowserRouter>
                <NavBar />
                <Switch>
                  <Route path='/' exact component={JobList} />
                  <Route path='/profile' exact component={Profile} />
                  <Route path='/companies' exact component={CompanyList} />
                  <Route path='/job-list' exact component={JobList} />
              {/*     <Route path='/login' exact component={Login} /> */}
                  <Route component={PageNotFound} />
                </Switch>
              </BrowserRouter>
            </>
          )}
        </div>
      </>
    );
  }
}
export default App;
//export default App;
