import React from "react";
import { Link } from "react-router-dom";
import JobBriefList from "./JobBriefList";
import SearchBarForLocation from "./SearchBarForLocation";
import SearchBarForName from "./SearchBarForName";
import Jobs from "../jobs.json";
import { fetchJobs, selectJob } from "../actions";
import { connect } from "react-redux";
class JobList extends React.Component {
  filterByName = (e) => {
    console.log("filterbyname", e);
    //this.setState({ filterJobs: e });
    // this.setState({ selectedJob: null });
    this.props.fetchJobs(e);
    this.props.selectJob(null);
  };
  filterByLocation = (e) => {
    console.log("filter by locaiton", e);
    this.props.fetchJobs(e);
    //  this.setState({ filterJobs: e });
    //  this.setState({ selectedJob: null });
    this.props.selectJob(null);
  };
  render() {
    return (
      <div className='ui grid'>
        <div className='sixteen wide column' style={{ maxHeight: "15vh" }}>
          <div className='ui grid'>
            <div className='six wide column'>
              <SearchBarForName
                jobs={Jobs}
                onSearchBarForName={this.filterByName}
              />
            </div>
            <div className='six wide column'>
              <SearchBarForLocation
                jobs={Jobs}
                onSearchBarForLocation={this.filterByLocation}
              />
            </div>
            <div className='four wide column' style={{ margin: "auto" }}>
              <Link className='ui button primary' to='/profile'>
                See Your Profile
              </Link>
            </div>
          </div>
        </div>
        <div className='sixteen wide column'>
          <JobBriefList
          //   jobList={this.state.filterJobs}
          //    onClick={this.onJobClick}
          //  selectedJob={this.state.selectedJob}
          />
        </div>
      </div>
    );
  }
}

export default connect(null, { fetchJobs, selectJob })(JobList);
