import React, { useState, useEffect, useRef } from "react";
import "../css/navBar.css";
import { Link, NavLink } from "react-router-dom";
import GoogleAuth from "./googleAuth/GoogleAuth";

import { connect } from "react-redux";
import DropDownUI from "./DropDownUI";
function NavBar(props) {
  const [showProfileDropDown, setShowProfileDropDown] = useState(false);
  const ref = useRef();
  const navData = [
    {
      href: "/",
      name: "Home",
    },
    {
      href: "/profile",
      name: "See Your Profile",
    },

    {
      href: "/companies",
      name: "See companies’ jobs",
    },
  ];
  /*   useEffect(() => {
    setIsSelected(
      navData.findIndex((f) => f.href === window.location.pathname)
    );
  }, [window.location.pathname]);
  const onNavClick = (index) => {
    setIsSelected(index);
  }; */
  useEffect(() => {
    const onBodyClick = (event) => {
      if (ref.current && !ref.current.contains(event.target)) {
        setShowProfileDropDown(false);
      }
    };

    document.body.addEventListener("click", onBodyClick);

    return () => {
      document.body.removeEventListener("click", onBodyClick);
    };
  }, []);
  const renderNav = navData.map((nav, index) => {
    //  const active = index === isSelected ? "active" : "";
    return (
      <span key={nav.href}>
        {/* <Link key={nav.href} className={`${active} item`} to={nav.href}> */}
        <NavLink
          exact={true}
          key={nav.href}
          activeClassName='active'
          className='item'
          to={nav.href}
        >
          {nav.name}
        </NavLink>
      </span>
    );
  });
  return (
    <React.Fragment>
      <div
        className='ui secondary pointing menu'
        style={{
          position: "sticky",
          top: "0",
          padding: "2px",
          zIndex: "6",
          background: "white",
        }}
      >
        <span>
          <Link to='/'>
            <h2 className='NavBar__logo'>Job Portal</h2>
          </Link>
        </span>

        {renderNav}

        <div className='right menu'>
          <div className='ui item'>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              {props.isSignIn ? (
                /*  <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <h3>{props.userProfile.name}</h3>
                  <img
                    className='ui mini circular image'
                    src={props.userProfile.profileAvatar}
                    alt='avatar'
                  />
                </div> */
                <div
                  className={`ui inline dropdown ${
                    showProfileDropDown ? "active visible" : ""
                  }`}
                >
                  <div
                    ref={ref}
                    className='text'
                    onClick={() => setShowProfileDropDown(!showProfileDropDown)}
                  >
                    <img
                      alt='avatar'
                      className='ui avatar image'
                      src={props.userProfile.profileAvatar}
                    />
                    {props?.userProfile.name}
                  </div>
                  <i className='dropdown icon'></i>
                  <div
                    className={`menu transition ${
                      showProfileDropDown ? "visible" : ""
                    }`}
                  >
                    <div className='item' style={{ textAlign: "center" }}>
                      <br />
                      <img
                        alt='avatar'
                        className='ui massive circular image'
                        src={props.userProfile.profileAvatar}
                      />
                      <h3> {props?.userProfile?.name}</h3>
                      <h4> {props?.userProfile?.gmail}</h4>
                      <GoogleAuth />
                    </div>
                  </div>
                </div>
              ) : (
                <GoogleAuth />
              )}
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
const mapStateToProps = (state) => {
  return {
    isSignIn: state.auth.isSignedIn,
    userProfile: state.auth.user?.profile,
  };
};
export default connect(mapStateToProps)(NavBar);
