import React from "react";

function PageNotFound() {
  return (
    <div className='ui container'>
      <div
        style={{
          height: "100vh",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <h2>Page Not Found</h2>
      </div>
    </div>
  );
}

export default PageNotFound;
