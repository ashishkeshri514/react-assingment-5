import { combineReducers, createStore } from "redux";
import authReducer from "./authReducer";
import { companiesReducer, selectedCompanyReducer } from "./companiesReducer";
import { fetchJobReducer, selectedJobReducer } from "./jobReducer";
import { profileReducer } from "./profileReducer";
import { composeWithDevTools } from "redux-devtools-extension";

const rootReducer = combineReducers({
  companies: companiesReducer,
  selectedCompany: selectedCompanyReducer,
  jobs: fetchJobReducer,
  selectedJob: selectedJobReducer,
  profile: profileReducer,
  auth: authReducer,
});

const store = createStore(rootReducer, composeWithDevTools());
export default store;
